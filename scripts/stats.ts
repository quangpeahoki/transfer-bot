import "@nomiclabs/hardhat-ethers";
import "@openzeppelin/hardhat-upgrades";
import { ethers } from "hardhat";

async function main() {
  const [owner] = await ethers.getSigners();
  console.log("Account:", owner.address);
  console.log("Account balance:", (await owner.getBalance()).toString());

  const calculateTPS = async () => {
    const startBlockNumber = 428597;
    const startBlock = await ethers.provider.getBlock(startBlockNumber);
    const startTime = startBlock.timestamp;
    console.log("startBlock:", startBlockNumber);

    let maxTxPerBlock = 0;
    let minTxPerBlock = startBlock.transactions.length;

    let totalTxn = 0;

    const endBlockNumber = 429196; //await ethers.provider.getBlockNumber();
    const endBlock = await ethers.provider.getBlock(endBlockNumber);
    const endTime = endBlock.timestamp;
    console.log("endBlock:", endBlockNumber);
    for (let i = startBlockNumber; i < endBlockNumber; i++) {
      const block = await ethers.provider.getBlock(i);
      console.log(i, "transactions:", block.transactions.length);
      totalTxn += block.transactions.length;

      if (block.transactions.length > maxTxPerBlock) {
        maxTxPerBlock = block.transactions.length;
      }

      if (block.transactions.length < minTxPerBlock && block.transactions.length != 0) {
        minTxPerBlock = block.transactions.length;
      }
    }

    const tps = totalTxn / (endTime - startTime);
    const totalBlock = endBlockNumber - startBlockNumber + 1;
    console.log("AVG TPS:", tps);
    console.log("MAX TPS:", maxTxPerBlock / 3);
    console.log("MIN TPS:", minTxPerBlock / 3);

    console.log("maxTxPerBlock:", maxTxPerBlock);
    console.log("minTxPerBlock:", minTxPerBlock);
    console.log("avgTxPerBlock:", totalTxn / totalBlock);

    console.log("totalBlock:", totalBlock);
    console.log("totalTx:", totalTxn);
  }

  const checkTxn = async () => {
    const txn = await ethers.provider.getTransaction("0x5aef87008a2b55944d5fd3e1a20fe7d465839a3d645a71f8fa1ea3b45ea5125d");
    console.log(txn)
  }

  const checkBlockTimestamp = async () => {
    const startBlockNumber = await ethers.provider.getBlockNumber();
    const startBlock = await ethers.provider.getBlock(startBlockNumber);
    const startTime = startBlock.timestamp;

    const endBlock = await ethers.provider.getBlock(startBlockNumber + 1);
    const endTime = endBlock.timestamp;


    console.log(endTime - startTime);
  }

  await calculateTPS();
  // await checkBlockTimestamp();
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});

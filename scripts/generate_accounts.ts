import "@nomiclabs/hardhat-ethers";
import "@openzeppelin/hardhat-upgrades";
import {ethers, upgrades} from "hardhat";
import * as fs from 'fs';

async function main() {
    const [owner] = await ethers.getSigners();
    console.log("Account:", owner.address);
    console.log("Account balance:", (await owner.getBalance()).toString());

    const generate = async () => {
        for (let i = 0; i < 10; i++) {
            let random = await ethers.Wallet.createRandom();
            fs.writeFileSync("./data/accounts.txt", random.privateKey + "\n", {
                flag: 'a+',
            });
            console.log(random.privateKey);
        }
    }

    await generate();
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
    });

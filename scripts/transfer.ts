import "@nomiclabs/hardhat-ethers";
import "@openzeppelin/hardhat-upgrades";
import {ethers, upgrades} from "hardhat";
import * as fs from 'fs';

async function main() {
    const [owner] = await ethers.getSigners();
    console.log("Account:", owner.address);
    console.log("Account balance:", (await owner.getBalance()).toString());

    const gasLimit = 21000;
    const gasPrice = await ethers.provider.getGasPrice();
    const accounts = fs.readFileSync("./data/accounts.txt").toString().split("\n");

    const transfer = async () => {
        for (let i = 0; i < accounts.length; i++) {
            if (accounts[i] == "") {
                continue
            }
            let wallet = new ethers.Wallet(accounts[i], ethers.provider);
            const tnx = await owner.sendTransaction({
                to: wallet.address,
                value: ethers.utils.parseEther("0.05")
            });
            console.log(i, "Txn:", tnx.hash);
        }
    }

    const transferBack = async () => {
        for (let i = 0; i < accounts.length; i++) {
            if (accounts[i] == "") {
                continue
            }
            let wallet = new ethers.Wallet(accounts[i], ethers.provider);

            let balance = await wallet.getBalance();
            let amount = balance.sub(gasPrice.mul(gasLimit));
            if (amount.lte(0)) {
                continue
            }

            const tnx = await wallet.sendTransaction({
                to: owner.address,
                value: amount,
                gasLimit: gasLimit,
                gasPrice: gasPrice,
            });
            console.log(i, "Txn back:", tnx.hash);
        }
    }

    // await transfer();
    await transferBack();
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
    });
